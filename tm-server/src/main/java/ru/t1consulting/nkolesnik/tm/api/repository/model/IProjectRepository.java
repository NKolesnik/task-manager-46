package ru.t1consulting.nkolesnik.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    List<Project> findAll(@Nullable Sort sort);

    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    List<Project> findAll(@Nullable Comparator comparator);

    List<Project> findAll(@Nullable String userId, @Nullable Comparator comparator);

}
