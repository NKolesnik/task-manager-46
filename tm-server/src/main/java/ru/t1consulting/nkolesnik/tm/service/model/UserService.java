package ru.t1consulting.nkolesnik.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.model.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.repository.model.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.model.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.model.UserRepository;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }


    @Override
    public void add(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void addALl(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            for (User user : users)
                repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            repository.clear();
            for (User user : users)
                repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.getSize();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @Nullable final List<User> users;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            users = repository.findAll();
            if (users.isEmpty()) return Collections.emptyList();
            return users;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            repository.clear();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (role == null) throw new RoleIsEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(role);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final User user = new User();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            user.setRole(role);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.findByLogin(login);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.findByEmail(email);
        } finally {
            em.close();
        }
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            @Nullable final User user = findById(id);
            if (user == null) throw new UserNotFoundException();
            repository.setPassword(user, password);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            result = repository.findById(id);
            if (result == null) throw new UserNotFoundException();
            repository.updateUser(id, firstName, middleName, lastName);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return result;
    }

    @Override
    public void update(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            repository.update(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @Nullable final User result;
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(em);
            result = repository.findById(user.getId());
            if (result == null) throw new UserNotFoundException();
            @NotNull final String userId = result.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            repository.remove(result);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            @Nullable final User repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.removeByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.isLoginExist(login);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.isEmailExist(email);
        } finally {
            em.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            @Nullable final User repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.lockUserByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            em.getTransaction().begin();
            @Nullable final User repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.unlockUserByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}