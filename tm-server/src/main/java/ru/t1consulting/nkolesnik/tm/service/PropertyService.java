package ru.t1consulting.nkolesnik.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "359751";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "842685";

    @NotNull
    public static final String EMPTY_VALE = "---";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    public static final String DATABASE_CONNECTION_STRING_KEY = "database.url";

    @NotNull
    public static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    public static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    public static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    public static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    public static final String DATABASE_HBM2DDL_KEY = "database.hbm2ddl_auto";

    @NotNull
    public static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    public static final String DATABASE_FORMAT_SQL_KEY = "database.format_sql";

    @NotNull
    public static final String CACHE_USE_SECOND_LVL_CACHE_KEY = "cache.use_second_lvl_cache";

    @NotNull
    public static final String CACHE_PROVIDER_CONFIG_FILE_KEY = "cache.provider_config_file";

    @NotNull
    public static final String CACHE_USE_QUERY_CACHE_KEY = "cache.use_query_cache";

    @NotNull
    public static final String CACHE_USE_MIN_PUTS_KEY = "cache.use_min_puts";

    @NotNull
    public static final String CACHE_REGION_PREFIX_KEY = "cache.region_prefix";

    @NotNull
    public static final String CACHE_REGION_FACTORY_CLASS_KEY = "cache.region.factory_class";

    @NotNull
    public static final String CACHE_HAZELCAST_USE_LITE_MEMBER_KEY = "cache.hazelcast.use_lite_member";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }


    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseConnectionString() {
        return getStringValue(DATABASE_CONNECTION_STRING_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_KEY);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2Ddl() {
        return getStringValue(DATABASE_HBM2DDL_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SESSION_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getCacheUseSecondLvlCache() {
        return getStringValue(CACHE_USE_SECOND_LVL_CACHE_KEY);
    }

    @NotNull
    @Override
    public String getCacheProviderConfigFile() {
        return getStringValue(CACHE_PROVIDER_CONFIG_FILE_KEY);
    }

    @NotNull
    @Override
    public String getCacheUseQueryCache() {
        return getStringValue(CACHE_USE_QUERY_CACHE_KEY);
    }

    @NotNull
    @Override
    public String getCacheUseMinPuts() {
        return getStringValue(CACHE_USE_MIN_PUTS_KEY);
    }

    @NotNull
    @Override
    public String getCacheRegionPrefix() {
        return getStringValue(CACHE_REGION_PREFIX_KEY);
    }

    @NotNull
    @Override
    public String getCacheRegionFactoryClass() {
        return getStringValue(CACHE_REGION_FACTORY_CLASS_KEY);
    }

    @NotNull
    @Override
    public String getCacheHazelcastUseLiteMember() {
        return getStringValue(CACHE_HAZELCAST_USE_LITE_MEMBER_KEY);
    }

}