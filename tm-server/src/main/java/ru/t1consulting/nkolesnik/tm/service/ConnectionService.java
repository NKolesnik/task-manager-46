package ru.t1consulting.nkolesnik.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private static final String HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> properties = new HashMap<>();
        properties.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        properties.put(Environment.URL, propertyService.getDatabaseConnectionString());
        properties.put(Environment.USER, propertyService.getDatabaseUsername());
        properties.put(Environment.PASS, propertyService.getDatabasePassword());
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2Ddl());
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getCacheUseSecondLvlCache());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfigFile());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getCacheUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getCacheUseMinPuts());
        properties.put(HAZELCAST_USE_LITE_MEMBER, propertyService.getCacheHazelcastUseLiteMember());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(properties);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources metadataSources = new MetadataSources(registry);
        metadataSources.addAnnotatedClass(User.class);
        metadataSources.addAnnotatedClass(UserDTO.class);
        metadataSources.addAnnotatedClass(Project.class);
        metadataSources.addAnnotatedClass(ProjectDTO.class);
        metadataSources.addAnnotatedClass(Task.class);
        metadataSources.addAnnotatedClass(TaskDTO.class);
        @NotNull final Metadata metadata = metadataSources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}